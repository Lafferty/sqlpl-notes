create or replace function p7() returns void as $$
    declare
        c1 cursor for select Account, Cname, Cbalance, Crlimit from Customer;
            Cnum char(2);
            Cnam char(255);
            fee float;
            Cbal float;
            Nbal float;
            Clim float;
            Oval float;
    begin
        open c1;
        loop
            fetch c1 into Cnum, Cnam, Cbal, Clim; exit when not found;
            raise notice '============';
            raise notice 'Account Number: %', Cnum;
            raise notice '------------';

            if Cbal <= Clim then
                raise notice 'NO FEE';
            else
                Oval := Cbal - Clim;
                fee := Oval * 0.10;
                Nbal := Cbal + fee;
                update Customer set Cbalance = Nbal
                    where Account = Cnum;
                raise notice 'Customer Name = %', Cnam;
                raise notice 'New Balance = %', Nbal;
            end if;
            raise notice '============';
        end loop;
        close c1;
    end;
$$ language plpgsql;
