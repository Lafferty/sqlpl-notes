create or replace function p1(name char(255)) returns void as $$
    declare
        c1 cursor for select Tno, Vname, T_Date, Amount from Customer, Vendor, Transaction
            where Cname = name and Transaction.Vno = Vendor.Vno and Transaction.Account = Customer.Account;
            Tno char(2);
            Vname char(255);
            T_Date date;
            Amount float;
    begin
        open c1;
        loop
            fetch c1 into Tno, Vname, T_Date, Amount; exit when not found;
            raise notice '============';
            raise notice 'Transaction: %', Tno;
            raise notice '------------';
            raise notice 'Vendor = %', Vname;
            raise notice 'Date = %', T_Date;
            raise notice 'Amount =  %', Amount;
            raise notice '============';
        end loop;
        close c1;
    end;
$$ language plpgsql;
