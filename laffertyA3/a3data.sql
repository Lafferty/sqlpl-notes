create table Vendor(Vno char(2), Vname char(255), City char(255), Vbalance float, primary key (Vno));
create table Customer(Account char(2), Cname char (255), Province char (3), Cbalance float, Crlimit integer, primary key(Account));
create table Transaction(Tno char(2), Vno char(2), Account char(2), T_Date date, Amount float, primary key (Tno), foreign key (Vno) references Vendor(Vno), foreign key (Account) references Customer(Account));

insert into Vendor (Vno, Vname, City, Vbalance) VALUES ('V1', 'Sears', 'Toronto', '200.00');
insert into Vendor (Vno, Vname, City, Vbalance) VALUES ('V2', 'Walmart', 'Waterloo', '671.05');
insert into Vendor (Vno, Vname, City, Vbalance) VALUES ('V3', 'Esso', 'Windsor', '0.00');
insert into Vendor (Vno, Vname, City, Vbalance) VALUES ('V4', 'Esso', 'Waterloo', '225.00');

insert into Customer (Account, Cname, Province, Cbalance, Crlimit) VALUES ('A1', 'Smith', 'ONT', '2515.00', '2000');
insert into Customer (Account, Cname, Province, Cbalance, Crlimit) VALUES ('A2', 'Jones', 'BC', '2014.00', '2500');
insert into Customer (Account, Cname, Province, Cbalance, Crlimit) VALUES ('A3', 'Doc', 'ONT', '150.00', '1000');

insert into Transaction (Tno, Vno, Account, T_Date, Amount) VALUES ('T1', 'V2', 'A1', '2016-07-15', '1325.00');
insert into Transaction (Tno, Vno, Account, T_Date, Amount) VALUES ('T2', 'V2', 'A3', '2015-12-16', '1900.00');
insert into Transaction (Tno, Vno, Account, T_Date, Amount) VALUES ('T3', 'V3', 'A1', '2016-09-01', '2500.00');
insert into Transaction (Tno, Vno, Account, T_Date, Amount) VALUES ('T4', 'V4', 'A2', '2016-03-20', '1613.00');
insert into Transaction (Tno, Vno, Account, T_Date, Amount) VALUES ('T5', 'V4', 'A3', '2016-07-31', '3312.00');
