create or replace function p2(name char(255)) returns void as $$
    declare
        c1 cursor for select Cname, Customer.Account, Province from Customer, Vendor, Transaction
            where Vname = name and Transaction.Vno = Vendor.Vno and Transaction.Account = Customer.Account;
            Cname char(255);
            Cno char(2);
            Province char(3);
    begin
        open c1;
        loop
            fetch c1 into Cname, Cno, Province; exit when not found;
            raise notice '============';
            raise notice 'Customer Number: %', Cno;
            raise notice '------------';
            raise notice 'Customer Name = %', Cname;
            raise notice 'Province = %', Province;
            raise notice '============';
        end loop;
        close c1;
    end;
$$ language plpgsql;
