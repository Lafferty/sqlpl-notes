create or replace function p6() returns void as $$
    declare
        c1 cursor for select Vno, Vname, Vbalance from Vendor;
            Vnum char(2);
            Vnam char(255);
            fee float;
            Vbal float;
            Nbal float;
    begin
        open c1;
        loop
            fetch c1 into Vnum, Vnam, Vbal; exit when not found;
            raise notice '============';
            raise notice 'Vendor Number: %', Vnum;
            raise notice '------------';
            fee := Vbal * 0.04;
            Nbal := Vbal - fee;
            update Vendor set Vbalance = Nbal
                where Vno = Vnum;
            raise notice 'Vendor Name = %', Vnam;
            raise notice 'Fee Charged = %', fee;
            raise notice 'New Balance = %', Nbal;
            raise notice '============';
        end loop;
        close c1;
    end;
$$ language plpgsql;
