create or replace function p8(Tnum char(2), Vnum char(2), Accnum char(2), Amnt float) returns void as $$
    declare
        Curdate date;
        CurrAmountV float;
        CurrAmountC integer;
        NbalV float;
        NbalC integer;
    begin
        select CURRENT_DATE into Curdate;
        insert into Transaction (Tno, Vno, Account, T_Date, Amount) values (Tnum, Vnum, Accnum, Curdate, Amnt);

        select Cbalance into CurrAmountC from Customer where Account = Accnum;
        NbalC := CurrAmountC + Amnt;
        update Customer set Cbalance = NbalC
            where Account = Accnum;

        select Vbalance into CurrAmountV from Vendor where Vnum = Vno;
        NbalV := CurrAmountV + Amnt;
        update Vendor set Vbalance = NbalV
            where Vno = Vnum;

        raise notice 'Transaction: %', Tnum;
        raise notice 'Customer %: Cbalance = %', Accnum, NbalC;
        raise notice 'Vendor %: Vbalance = %', Vnum, NbalV;
    end;
$$ language plpgsql;
