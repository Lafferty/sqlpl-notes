create or replace function p4() returns void as $$
    declare
        c1 cursor for select Customer.Account, Cname from Customer;
            Cno char(2);
            name char(255);
            tnum char(2);
            Vnum char(2);
            Vna char(255);
            amnt float;
    begin
        open c1;
        loop
            fetch c1 into Cno, name; exit when not found;
            raise notice '============';
            raise notice 'Account Number: %', Cno;
            raise notice '------------';
            select Tno, Vno, Amount into tnum, Vnum, amnt from Transaction
            where T_Date = (select max(T_Date)
                from transaction
                where Cno = Transaction.Account);
            if not found then
                raise notice 'NO TRANSACTION';
            else
                select Vname into Vna from Vendor
                where Vnum = Vno;
                raise notice 'Customer Name = %', name;
                raise notice 'Amount = %', amnt;
                raise notice 'Vendor Name = %', Vna;
            end if;
            raise notice '============';
        end loop;
        close c1;
    end;
$$ language plpgsql;
