create or replace function p3(Acc char(2), Cn char(255), Prov char(3), Crlim integer) returns void as $$
    begin
        insert into Customer (Account, Cname, Province, Cbalance, Crlimit) values (Acc, Cn, Prov, 0, Crlim);
    end;
$$ language plpgsql;
