create or replace function p5() returns void as $$
    declare
        c1 cursor for select Vno, Vname from Vendor;
            Vnum char(2);
            Vnam char(255);
            Tsum float;
            Vbal float;
            Nbal float;
    begin
        open c1;
        loop
            fetch c1 into Vnum, Vnam; exit when not found;
            raise notice '============';
            raise notice 'Vendor Number: %', Vnum;
            raise notice '------------';
            select sum(Amount) into Tsum from Transaction
                where Vno = Vnum;
            if Tsum is NULL then
                raise notice 'NO TRANSACTIONS';
            else
                select Vbalance into Vbal from Vendor
                where Vno = Vnum;
                Nbal := Tsum + Vbal;
                update Vendor set Vbalance = Nbal
                    where Vno = Vnum;
                raise notice 'Vendor Name = %', Vnam;
                raise notice 'New Balance = %', Nbal;
            end if;
            raise notice '============';
        end loop;
        close c1;
    end;
$$ language plpgsql;
